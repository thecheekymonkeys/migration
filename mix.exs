defmodule Migration.Mixfile do
  use Mix.Project

  def project do
    [
      app: :migration,
      version: "0.1.0",
      elixir: "~> 1.5",
      escript: escript_config(),
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 0.13"},
      {:poison, "~> 3.1"},
      {:floki, "~> 0.19.0"},
      {:postgrex, "~> 0.12.2"}
    ]
  end

  # Entry point for binary
  defp escript_config do
    [ main_module: Migration.CLI ]
  end

end
