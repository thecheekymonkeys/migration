defmodule Migration.CLI do

  # The default number of events to pull
  @default_count 10

  @moduledoc """
  Handle user arguments from the command line. Parse -h --help as
  switches
  """
  def run(argv) do
    argv
    |> parse_args
    |> process
    |> decode_response
    #    |> write_to_file
    #|> handle_write
    |> write_to_database
  end

  @doc """
  `argv` can be -h or --help, which returns :help

  Otherwise, it is a latitude, longitude, radius, start date, and, end date count of
  events to fetch from the meetup api

  Return a tuple of `{ lat, lng, radius, start_date, end_date }`, 
  or `:help` if help was given
  """
  def parse_args(argv) do
    
    parsed = OptionParser.parse(argv, switches: [help: :boolean],
                             aliases: [ h: :help ])

    case parsed do

      { _, [ help: true ], _, _ } -> :help
      { _, [ lat, lng, radius, start_date, end_date ], _ } -> { lat, lng, radius, start_date, end_date }
      _ -> :help

    end 
  end 

  def process(:help) do
    IO.puts """
    usage: migration <lat> <lng> <radius> <start_date> <end_date> [ count | #{@default_count} ]
    """
  end

  def process({ lat, lng, radius, start_date, end_date }) do
    Migration.Meetup.fetch_events(lat, lng, radius, start_date, end_date)
  end

  def decode_response({ :ok, body }), do: body

  def decode_response({ :error, error }) do
    {_, message} = List.keyfind(error, "message", 0)
    IO.puts "Error fetching events: #{message}"
    System.halt(2)
  end 


  def write_to_file(contents) do
    Path.absname("./dump.json")
    |> File.write!(Poison.encode!(contents), [:binary])
  end

  def write_to_database(events) do
    Migration.Database.insert_events(events)
  end

  def handle_write({:error, message }) do
    System.halt(2)
  end

  def handle_write(:ok),
    do: IO.puts("Success!")

end
