defmodule Migration.FunCheap do
  @moduledoc """
  Fetch a list of events from funcheap by scraping the website
  """
  
  @base_url Application.get_env(:migration, :fun_cheap_url)

  def fetch_events(date) do
    HTTPoison.get("#{@base_url}/#{date}/")
    |> handle_response
    |> Floki.find("div.tanbox")
    |> Enum.map(&Migration.FunCheap.parse_event/1)
    |> IO.inspect 
  end

  def parse_event(event) do
    title_map = parse_title(List.first(Floki.find(event, "a")))
    { _, _, children } = event
    body_map = %{ 
      "description": Floki.text(List.last(children)),
      "image": parse_image(event),
      "time": parse_time(event)
    }
    Map.merge(title_map, body_map)
  end  

  defp parse_image(event) do
    case List.first(Floki.find(event, "img")) do
      { tag, attr, children } -> 
        { _, url } = List.first(attr)
        url
      _ -> ""
    end 
  end

  defp parse_time(event) do
    case List.first(Floki.find(event, "div.meta")) do
      { _, _, children } -> List.first(children)
      |> String.replace(<<194, 150>>, <<>>, [])
      |> String.trim_trailing
      _ -> ""
    end 
  end


  defp parse_title({ tag, attrs, children }) do 
    case attrs do
      [ url_attr, _, title_attr ] -> 
        { _, title_str } = title_attr
        [ title, location ] = String.split(title_str, " | ")
        { _, url } = url_attr
        %{ "title": title, "location": location, "url": url }
    end 
  end

  defp parse_title(nil), 
    do: %{ "title": "" } 

  defp handle_response({:ok, %HTTPoison.Response{status_code: 200, body: body}}),
    do: body

  defp handle_response({:ok, %HTTPoison.Response{status_code: 404}}),
    do: { :error, "uh oh" }

  defp handle_response({:error, %HTTPoison.Error{reason: reason}}),
    do: { :error, reason }
    
end
