defmodule Migration.Database do

  @db_host Application.get_env(:migration, :db_host)
  @db_name Application.get_env(:migration, :db_name)
  @db_user Application.get_env(:migration, :db_user)
  @db_password Application.get_env(:migration, :db_password)
  @table_name Application.get_env(:migration, :events_table)

  @moduledoc """
  Handle writing events to the database
  """

  @doc """
  `events` is a collection of events
  """
  def insert_events(events) do
    {:ok, pid} = Postgrex.start_link(
      hostname: @db_host,
      username: @db_user,
      password: @db_password,
      database: @db_name
    )
    Enum.each(events, fn event -> insert_event(event, pid) end)
  end

  @doc """
  `event` a tuple that contains a time, location, and description 
  """
  defp insert_event(event, pid) do

    name = Map.get(event, "name")
    url = Map.get(event, "url")
    description = Map.get(event, "description")
    address = Map.get(event, "address")
    local_date = Map.get(event, "local_date")
    local_time = Map.get(event, "local_time")
    preview_photo = Map.get(event, "photo")

    IO.inspect Postgrex.query(
      pid, 
      "INSERT INTO events (name, url, description, address, local_date, local_time, preview_photo) VALUES ($1, $2, $3, $4, $5, $6, $7)",
      [name, url, description, address, local_date, local_time, preview_photo]
    )

  end

end
