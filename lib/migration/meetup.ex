defmodule Migration.Meetup do
  @moduledoc """
  Fetch a list of events from the meetup api and return it
  """
  
  @api_key Application.get_env(:migration, :meetup_api_key)
  @base_url Application.get_env(:migration, :meetup_base_url)
  @events_url Application.get_env(:migration, :meetup_events_url)

  def fetch_events(lat, lng, radius, start_date, end_date) do
    meetup_url(@base_url, @events_url)
    |> HTTPoison.get([], [ {:params, [ 
      {:key, @api_key },
      {:lat, lat },
      {:lon, lng },
      {:radius, radius },
      {:start_date_range, start_date },
      {:end_date_range, end_date },
      {:fields, "featured_photo, plain_text_no_images_description" }
    ]}])
    |> handle_response
  end 

  def meetup_url(base, type),
    do: "#{base}/#{type}"


  @doc"""
  Parses the response into a standard format for the database. The 
  standard format is a map of the following form:
  [
    {
      name => name of the event,
      description => description of the event,
      url => source url for the event,
      address => address of the event,
      local_time => local time of the event,
      local_date => local date of the event
    }
  ]
  If data is not available, a nil is passed 
  """
  defp parse_events(events) do
    IO.inspect events
    Enum.map(events, fn event ->
      %{
        "name" => Map.get(event, "name"),
        "url" => Map.get(event, "link"),
        "description" => Map.get(event, "description"),
        ##"description" => Map.get(event, "plain_text_no_images_description"),
        "local_date" => Map.get(event, "local_date"),
        "local_time" => Map.get(event, "local_time"),
        "address" => parse_address(event),
        "photo" => parse_photo(event)
      }
    end
    )
  end

  @doc"""
  Extracts an address using fallbacks
  """
  defp parse_address(event) do
    venue = Map.get(event, "venue", %{})
    case venue do
      nil -> nil
      _ -> "#{Map.get(venue, "address_1")}, #{Map.get(venue, "city")}"
    end
  end

  @doc"""
  Extracts a photo using fallbacks
  """
  defp parse_photo(event) do
    photo = Map.get(event, "featured_photo", %{})
    case photo do
      nil -> nil
      _ -> Map.get(photo, "photo_link")
    end
  end
  
  defp handle_response({ :ok, %{ status_code: 200, body: body } }) do
   Map.get(Poison.Parser.parse!(body), "events", [])
   ## |> Enum.filter(fn event -> Map.get(event, "visibility") == "public" end)
   |> parse_events
   |> (fn arg -> {:ok, arg} end).()
  end

  defp handle_response({ :error, %{ status_code: 200, body: body } }),
    do: { :error, Poison.Parser.parse!(body) }

end 
