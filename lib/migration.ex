defmodule Migration do
  @moduledoc """
  Documentation for Migration.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Migration.hello
      :world

  """
  def hello do
    :world
  end

  def run do
    Migration.CLI.run([37.7749, -122.431297,  10, "2017-12-06T00:00:01", "2017-12-06T23:59:59"])
  end

  def run_scraper(date), do:
    Migration.FunCheap.fetch_events(date)
    


end
